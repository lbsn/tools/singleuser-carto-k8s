FROM jupyterhub/k8s-singleuser-sample:1.1.3-n269.hf706eaa2

LABEL maintainer="Alexander Dunkel <alexander.dunkel@tu-dresden.de>"

# build time args
ARG ENVIRONMENT_FILE=environment_default.yml
ARG WORKER_ENV_NAME=worker_env

# create conda paths to be sourced
ENV CONDA_ACTIVATE_PATH=/opt/conda/bin/activate \
    WORKER_ENV_PATH=/opt/conda/envs/$WORKER_ENV_NAME/

COPY $ENVIRONMENT_FILE /tmp/$ENVIRONMENT_FILE

### Follows largely jupyter/minimal-notebook 
### and partly jupyter/scipy-notebook

# select default shell
# Fix: https://github.com/hadolint/hadolint/wiki/DL4006
# Fix: https://github.com/koalaman/shellcheck/wiki/SC3014
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

# Install all OS dependencies for fully functional notebook server
RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
    # for cython: https://cython.readthedocs.io/en/latest/src/quickstart/install.html
    build-essential \
    # Common useful utilities
    git \
    nano-tiny \
    tzdata \
    unzip \
    vim-tiny \
    # Inkscape is installed to be able to convert SVG files
    inkscape \
    # git-over-ssh
    openssh-client \
    # less is needed to run help in R
    # see: https://github.com/jupyter/docker-stacks/issues/1588
    less && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
    
# Create alternative for nano -> nano-tiny
RUN update-alternatives --install /usr/bin/nano nano /bin/nano-tiny 10

# Update conda to latest and set channel_priority
RUN conda update --channel defaults --name base --yes conda \
 && conda config --set channel_priority strict \
 && conda clean --all --force-pkgs-dirs --yes

# Add additional packages to base environment
RUN conda install --channel conda-forge \
    --prefix="${CONDA_DIR}" \
    --yes \
    jupyter_contrib_nbextensions \
    jupyter_nbextensions_configurator \
    jupytext \
    jupyterlab_widgets \
    'nbconvert=6.5.*' \
    pyviz_comms  \
    && fix-permissions "${CONDA_DIR}"  \
    && fix-permissions "/home/${NB_USER}"

# Set the default kernel for users
ENV JUPYTER_CONFIG=/etc/jupyter/jupyter_notebook_config.py
RUN echo "c.MultiKernelManager.default_kernel_nameUnicode=$WORKER_ENV_NAME" >>$JUPYTER_CONFIG;

USER ${NB_UID}

# Install user kernel environment (worker_env)
RUN conda env create --file /tmp/$ENVIRONMENT_FILE --name $WORKER_ENV_NAME --quiet  \
    && source $CONDA_ACTIVATE_PATH $WORKER_ENV_PATH \
    && conda install ipykernel --channel conda-forge \
    && conda clean --all --force-pkgs-dirs --yes \
    && conda deactivate \
    && fix-permissions "${CONDA_DIR}"  \
    && fix-permissions "/home/${NB_USER}"

COPY start-singleuser.sh /usr/local/bin/
 
# Switch back to jovyan to avoid accidental container runs as root
USER ${NB_UID}

WORKDIR "${HOME}"