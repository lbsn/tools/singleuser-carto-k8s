## Carto JupyterHub k8s Single User Image

This image is derived from the [JupyterCarto Docker Container][jupyter-carto] 
and extends the [JupyterHub k8s-singleuser-sample][k8s-singleuser-sample] image
with cartographic packages.

----

## Use Docker Image

Specify the image in your helm `config.yml`:
```
singleuser:
  image:
    name: registry.gitlab.vgiscience.org/lbsn/tools/singleuser-carto-k8s:latest
```

Two tags are available:
- `latest`
- `mapnik`

## Tag image

After each change, update the git tag:
```
git tag -a v0.1.0 -m "Initial version tag"
VERSION=$(git describe --tags --abbrev=0)
```

This will be used to tag images.

## Build Docker Image

Without cache:
```bash
docker build --no-cache \
    --progress=plain \
    -t registry.gitlab.vgiscience.org/lbsn/tools/singleuser-carto-k8s:$VERSION .
```

or with cache:
```bash
docker build \
    --progress=plain \
    -t registry.gitlab.vgiscience.org/lbsn/tools/singleuser-carto-k8s:$VERSION .
docker push \
    registry.gitlab.vgiscience.org/lbsn/tools/singleuser-carto-k8s:$VERSION
```

## Add Mapnik and tag image

[mapnik/Dockerfile](mapnik/Dockerfile) builds up on the above and adds Mapnik.

Optionally, change the tag from `:latest` to a specific one be used.

Build the image with:
```bash
docker build --no-cache \
    --progress=plain \
    --build-arg CVERSION=$VERSION \
    -t registry.gitlab.vgiscience.org/lbsn/tools/singleuser-carto-k8s:mapnik_$VERSION \
    ./mapnik
docker push \
    registry.gitlab.vgiscience.org/lbsn/tools/singleuser-carto-k8s:mapnik_$VERSION
```

And update your Kubernetes `config.yml` with the specific version tag to use.

[jupyter-carto]: https://gitlab.vgiscience.de/lbsn/tools/jupyterlab
[k8s-singleuser-sample]: https://github.com/jupyterhub/zero-to-jupyterhub-k8s/tree/main/images/singleuser-sample